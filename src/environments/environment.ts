// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebaseConfig : {
    apiKey: "AIzaSyByh_zYJV5yI3hlYZU7inURcDCfw46RG10",
    authDomain: "hello-shahar.firebaseapp.com",
    databaseURL: "https://hello-shahar.firebaseio.com",
    projectId: "hello-shahar",
    storageBucket: "hello-shahar.appspot.com",
    messagingSenderId: "501492839682",
    appId: "1:501492839682:web:c3427f0388f2e04fdafe1b"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
