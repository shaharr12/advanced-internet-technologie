import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { User } from '../interfaces/user';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
email:string;
password:string;
hasError:Boolean = false;
errorMessage:string;

  onRegister(){
    if(this.password.length < 8){
      this.hasError = true;
      this.errorMessage =  "The password must conation at least 8 charachters"
    } if (!this.email.includes("@gmail.com")) {
      this.hasError = true;
      this.errorMessage =  "The email invalid "
    } else {
      this.auth.register(this.email,this.password).catch(() => {
        this.hasError=true;
        this.errorMessage = "This email is alreday exsist, please choose new one"
      });
   
}
  }


  constructor(public auth:AuthService) { }

  ngOnInit(): void {}

}

