import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore'
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
 /*
books = [{title:'Alice in Wonderland', author:'Lewis Carrol',summary:'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley'},
        {title:'War and Peace', author:'Leo Tolstoy', summary:'the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'}, 
        {title:'The Magic Mountain', author:'Thomas Mann',summary:'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites'}];


    public addBooks() {
      setInterval(()=> this.books.push({title:'A new one', author:'New athor',summary:'shors summary'}),50000);
    }


        public getBooks(){
          const booksObservable = new Observable(observer => {
            setInterval(()=>observer.next(this.books),500)
          });
          return booksObservable;
        }

      */

      
       /*  
      getBooks(userId):Observable<any[]>{


      }
      */
     bookCollection: AngularFirestoreCollection;

      public getBooks(userId){
          this.bookCollection = this.db.collection(`users/${userId}/books`)
          return this.bookCollection.snapshotChanges().pipe(map(
            collection =>collection.map(

              document => {
                const data = document.payload.doc.data();
                data.id = document.payload.doc.id;
                return data;
              }
            )
          ))
      }



      deleteBook(Userid:string,id:string){
          this.db.doc(`users/${Userid}/books/${id}`).delete();
      }


     constructor(private db:AngularFirestore ){  }



      }
 
     