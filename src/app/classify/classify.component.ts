import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  season:string;
  errorMessage:string;
  

  favoriteSeason: string 
  seasons: string[] = ['BBC', 'CNN', 'NBC'];
  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {

    this.getSeason()
    if(!this.checkIfSeasonExists()){
      this.errorMessage = 'The url is not exsist'
    }
    
  }

  getSeason() {
    this.season = this.route.snapshot.params.bbc;
    this.season = this.season.toUpperCase();
    
  }
  
  checkIfSeasonExists() {
    return this.seasons.includes(this.season)
  }

  makeSeasonDefault(season) {

    return season === this.season;
    

  }

}



