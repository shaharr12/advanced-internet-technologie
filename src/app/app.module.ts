import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {MatRadioModule} from '@angular/material/radio';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSliderModule } from '@angular/material/slider';
import { BooksComponent } from './books/books.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import {MatExpansionModule} from '@angular/material/expansion';
import { ClassifyComponent } from './classify/classify.component';
import { HttpClientModule } from '@angular/common/http';
import { CityFormComponent } from './city-form/city-form.component';
import {MatSelectModule} from '@angular/material/select';
import { AngularFireModule } from '@angular/fire';
import {FormsModule} from '@angular/forms';
import { PostsComponent } from './posts/posts.component'; 
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import {MatInputModule} from '@angular/material/input';
import { RegisterComponent } from './register/register.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {AngularFirestoreModule } from '@angular/fire/firestore';
import { BookFormComponent } from './book-form/book-form.component'



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    TemperaturesComponent,
    ClassifyComponent,
    CityFormComponent,
    PostsComponent,
    LoginComponent,
    RegisterComponent,
    BookFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatCardModule,
    MatListModule,
    MatSliderModule,
    MatExpansionModule,
    HttpClientModule,
    MatRadioModule,
    MatSelectModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    MatInputModule,
    MatFormFieldModule,
    AngularFirestoreModule
    

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
