import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email:string;
  password:string;
  hasError:Boolean = false;
  errorMessage:string;

  onSubmit(){
      this.auth.login(this.email,this.password).catch(() => {
        this.hasError=true;
        this.errorMessage = "The password or email is worng"
      });
       
    }
      
        
        
  


  constructor(private auth:AuthService, private router:Router) { }

  ngOnInit(): void {
  }


}
  
