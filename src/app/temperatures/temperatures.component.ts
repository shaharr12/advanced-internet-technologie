import { Weather } from './../interfaces/weather';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WheaterService} from '../wheater.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  city:string; 
  temperature:number; 
  image:string; 
  lat:number;
  lon:number;
  country:string;
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  data:any;

  constructor(private route:ActivatedRoute, private weatherService:WheaterService) { }

  ngOnInit(): void {
    this.city = this.route.snapshot.params.city; 
    this.weatherData$ = this.weatherService.searchWeatherData(this.city); 
    this.weatherData$.subscribe(
      data => {
        this.data = data;
      }, 
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message; 
      }
    )
  }
}
