import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BooksComponent } from './books/books.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { ClassifyComponent} from './classify/classify.component';
import { CityFormComponent } from './city-form/city-form.component';
import { PostsComponent } from './posts/posts.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
 { path: 'books', component: BooksComponent},
 { path: 'temperatures/:city', component: TemperaturesComponent},
 { path: 'classify/:bbc', component: ClassifyComponent},
 { path: 'city', component: CityFormComponent },
 { path: 'posts', component: PostsComponent },
 { path: 'login', component: LoginComponent },
 { path: 'register', component: RegisterComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


